$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "ilogica_check/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "ilogica_check"
  s.version     = IlogicaCheck::VERSION
  s.authors     = ["Jaime Arroyo Navia"]
  s.email       = ["jaime@ilogica.cl"]
  s.homepage    = "https://www.ilogica.cl"
  s.summary     = "Get remote data of the application"
  s.description = ""
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", ">= 4.1"
end