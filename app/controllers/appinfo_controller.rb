class AppinfoController < ApplicationController



    def get_data
        if !IlogicaCheck.app_id.nil? and !IlogicaCheck.app_username.nil? and !IlogicaCheck.app_token.nil? and !params[:access_token].nil? and params[:access_token] == IlogicaCheck.app_token
            render json: {app_id: IlogicaCheck.app_id, ruby_version: RUBY_VERSION.dup, rails_version:  Rails.version, database_info: self.get_database_data,gems: self.get_app_info, }, status: :ok
        else
            render json: {error: 'not authenticated'}, status: :unauthorized
        end
    end

    def get_app_info
        gems =  self.my_local_gems.map{ |name, specs| [name, specs.map{ |spec| spec.version.to_s }.join(',')] }
    end

    def my_local_gems
        Gem::Specification.sort_by{ |g| [g.name.downcase, g.version] }.group_by{ |g| g.name }
    end

    def get_database_data
        final_data = {}
        if Rails.configuration.database_configuration[Rails.env]['adapter'].include? 'mysql'
            final_data['database_engine'] = 'mysql'
            data = ActiveRecord::Base.connection.select_rows("SHOW VARIABLES LIKE '%version%'")
            data.each do |label, data|
                final_data[label] =  data
            end
        elsif Rails.configuration.database_configuration[Rails.env]['adapter'].include? 'postgresql'
            final_data['database_engine'] = 'postgresql'
            final_data['version'] = ActiveRecord::Base.connection.select_value('SELECT version()')
        end
        return final_data
    end


end
