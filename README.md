# README

## Ilógica Check initializer

Para el correcto funcionamient debes agregar un intializer con los siguientes datos

```ruby
IlogicaCheck.setup do |config|
    config.app_id = 'app_id' # debes solitiar este dato
    config.app_username = 'app_username' # debes solitiar este dato
    config.app_token = 'app_token' # debes solitiar este dato
end  
```