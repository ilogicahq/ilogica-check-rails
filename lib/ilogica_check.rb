require "ilogica_check/engine"

module IlogicaCheck

    # app_id
    mattr_accessor :app_id
    @@app_id = nil

    # app_username
    mattr_accessor :app_username
    @@app_username = nil

    # app_password
    mattr_accessor :app_token
    @@app_token = nil

    def self.setup
        yield self
    end

end
